Specificații detaliate.

În vederea realizării acestei aplicații din cadrul proiectului am folosit ca limbaj 
de programare JavaScript, drept urmare ca mediu de programare am folosit Node JS. 
Pentru a dezvolta cu ușurință aplicația web, am ales framework-ul express. 
Am facut alegerile menționate deoarece aceste tehnologii de programare sunt foarte 
populare și se bucură de suportul unei comunități mari.
Pentru realizarea interfeței web am ales să folosesc limbajul de programare ReactJS.

Descrierea proiectului.

Atunci când utilizatorul accesează aplicația, acesta va fi întâmpinat de un meniu unde
va putea vizualiza documentele existente și va putea încărca noi documente. În cadrul 
funcției de listare a documentelor vor fi afișate următoarele câmpuri: numele, categoria,
id-ul unic de înregistrare și link-ul către google drive al documentului.
În momentul accesării de către user al zonei de încărcare, acesta va putea alege documentul
ce va fi încărcat precum și categoria acestuia. Dacă nu există categoria dorită, aceasta va
putea fi adăugată. După ce utilizatorul va urma toți pașii necesari, acesta va putea confirma
trimiterea documentului ce va putea fi accesat ulterior din meniul de căutare.